<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

	</div><!-- .site-content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		
		<div class="site-info">
			<div class="webdev" style="float:right">
			<a href="https://www.trokis.com/" target="_blank" title="Web Developer"><img alt="Web Developer" border="0" src="https://1.bp.blogspot.com/-nblfcAnt_EA/VH7Efe_jBPI/AAAAAAAAEuY/_HGtKGbcS5c/s32/trokis%2B-logo.png"></a>
		</div>
			<?php
				/**
				 * Fires before the Twenty Fifteen footer text for footer customization.
				 *
				 * @since Twenty Fifteen 1.0
				 */
				do_action( 'twentyfifteen_credits' );
			?>
			<a href="<?php echo esc_url( __( 'https://indavao.net/', 'twentyfifteen' ) ); ?>"><?php printf( __( '%s', 'twentyfifteen' ), 'InDavao.Net Business Directory' ); ?></a>
		</div><!-- .site-info -->
	</footer><!-- .site-footer -->

</div><!-- .site -->

<?php wp_footer(); ?>

</body>
</html>
