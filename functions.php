<?php

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

}

if ( ! function_exists( 'indavao_post_thumbnail' ) ) :
function indavao_post_thumbnail() {
	if ( post_password_required() || is_attachment() ) {
		return;
	}
	
	if( ! has_post_thumbnail() ) {
?>
	<div class="post-thumbnail">
		<img width="825" height="236" src="<?php echo get_stylesheet_directory_uri(); ?>/images/InDavao-bannerv2-825x236.png" class="attachment-post-thumbnail wp-post-image" alt="<?php echo get_the_title(); ?>">
	</div><!-- .post-thumbnail -->
<?php
	} else {
		if ( is_singular() ) {
		?>

		<div class="post-thumbnail">
			<?php the_post_thumbnail('post-thumbnail', array('id' => '_image2', 'itemprop' => 'image', 'alt' => get_the_title())); ?>
		</div><!-- .post-thumbnail -->
		<?php } else { ?>
		<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true">
			<?php
				the_post_thumbnail( 'post-thumbnail', array( 'alt' => get_the_title() ) );
			?>
		</a>
		<?php } 
	} // End is_singular()
}
endif;

add_action( 'pre_get_posts', 'indavao_pre_get_posts' );

function indavao_pre_get_posts($query) {
	
	if ($query->is_home() && $query->is_main_query()) {
		$query->set('post_type', 'establishment');
	}
}
