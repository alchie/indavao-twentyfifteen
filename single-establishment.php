<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			/*
			 * Include the post format-specific template for the content. If you want to
			 * use this in a child theme, then include a file called called content-___.php
			 * (where ___ is the post format) and that will be used instead.
			 */
			get_template_part( 'content', 'establishment' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

			// Previous/next post navigation.
			/*
			the_post_navigation( array(
				'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next', 'twentyfifteen' ) . '</span> ' .
					'<span class="screen-reader-text">' . __( 'Next post:', 'twentyfifteen' ) . '</span> ' .
					'<span class="post-title">%title</span>',
				'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Previous', 'twentyfifteen' ) . '</span> ' .
					'<span class="screen-reader-text">' . __( 'Previous post:', 'twentyfifteen' ) . '</span> ' .
					'<span class="post-title">%title</span>',
			) );
			*/
		// End the loop.
		endwhile;
		?>

		</main><!-- .site-main -->
		
		<?php 
					$street = get_post_meta(get_the_ID(), '_establishment_details_address', true);
					$address = implode(', ', array(
						get_the_title(),
						$street,
						get_post_meta(get_the_ID(), '_establishment_details_address_city', true),
						get_post_meta(get_the_ID(), '_establishment_details_address_region', true),
						'Philippines',
						get_post_meta(get_the_ID(), '_establishment_details_address_postal', true)
					));
					
					if( $street != '' ) {
				?>
		<div class="site-main new">
			<div class="hentry map">

				<iframe width="100%" height="500" frameborder="0" style="border:0"
src="https://www.google.com/maps/embed/v1/place?q=<?php echo urlencode($address); ?>&key=AIzaSyDBLtVV2Y6edCYAYYwf0WZgDn0Y5V0RqDs"></iframe>
			</div>
		</div>
		<?php } ?>
				
<?php 
				
				/* Social Media */
		
		$facebook = get_post_meta(get_the_ID(), '_establishment_social_media_facebook', true); 
		$google = get_post_meta(get_the_ID(), '_establishment_social_media_google', true);
		$twitter = get_post_meta(get_the_ID(), '_establishment_social_media_twitter', true);
		$linkedin = get_post_meta(get_the_ID(), '_establishment_social_media_linkedin', true);
		$youtube = get_post_meta(get_the_ID(), '_establishment_social_media_youtube', true);
		$vimeo = get_post_meta(get_the_ID(), '_establishment_social_media_vimeo', true);
		$blogger = get_post_meta(get_the_ID(), '_establishment_social_media_blogger', true);
		$behance = get_post_meta(get_the_ID(), '_establishment_social_media_behance', true);
		$deviantart = get_post_meta(get_the_ID(), '_establishment_social_media_deviantart', true);
		$digg = get_post_meta(get_the_ID(), '_establishment_social_media_digg', true);
		$dribble = get_post_meta(get_the_ID(), '_establishment_social_media_dribble', true);
		$pinterest = get_post_meta(get_the_ID(), '_establishment_social_media_pinterest', true);
		$stumbleupon = get_post_meta(get_the_ID(), '_establishment_social_media_stumbleupon', true);
		$tumblr = get_post_meta(get_the_ID(), '_establishment_social_media_tumblr', true);
		
		$social_media = array();
		if( $facebook != '' ) {
			$social_media[] = "<li><a href='{$facebook}' target='_blank'><img src='".get_stylesheet_directory_uri()."/images/social/facebook.png'></a></li>";
		}
		if( $google != '' ) {
			$social_media[] = "<li><a href='{$google}' target='_blank'><img src='".get_stylesheet_directory_uri()."/images/social/google.png'></a></li>";
		}
		if( $twitter != '' ) {
			$social_media[] = "<li><a href='{$twitter}' target='_blank'><img src='".get_stylesheet_directory_uri()."/images/social/twitter.png'></a></li>";
		}
		if( $linkedin != '' ) {
			$social_media[] = "<li><a href='{$linkedin}' target='_blank'><img src='".get_stylesheet_directory_uri()."/images/social/linkedin.png'></a></li>";
		}
		if( $youtube != '' ) {
			$social_media[] = "<li><a href='{$youtube}' target='_blank'><img src='".get_stylesheet_directory_uri()."/images/social/youtube.png'></a></li>";
		}
		if( $vimeo != '' ) {
			$social_media[] = "<li><a href='{$vimeo}' target='_blank'><img src='".get_stylesheet_directory_uri()."/images/social/vimeo.png'></a></li>";
		}
		if( $blogger != '' ) {
			$social_media[] = "<li><a href='{$blogger}' target='_blank'><img src='".get_stylesheet_directory_uri()."/images/social/blogger.png'></a></li>";
		}
		if( $behance != '' ) {
			$social_media[] = "<li><a href='{$behance}' target='_blank'><img src='".get_stylesheet_directory_uri()."/images/social/behance.png'></a></li>";
		}
		if( $deviantart != '' ) {
			$social_media[] = "<li><a href='{$deviantart}' target='_blank'><img src='".get_stylesheet_directory_uri()."/images/social/deviantart.png'></a></li>";
		}
		if( $digg != '' ) {
			$social_media[] = "<li><a href='{$digg}' target='_blank'><img src='".get_stylesheet_directory_uri()."/images/social/digg.png'></a></li>";
		}
		if( $dribble != '' ) {
			$social_media[] = "<li><a href='{$dribble}' target='_blank'><img src='".get_stylesheet_directory_uri()."/images/social/dribble.png'></a></li>";
		}
		
		if( $pinterest != '' ) {
			$social_media[] = "<li><a href='{$pinterest}' target='_blank'><img src='".get_stylesheet_directory_uri()."/images/social/pinterest.png'></a></li>";
		}
		if( $stumbleupon != '' ) {
			$social_media[] = "<li><a href='{$stumbleupon}' target='_blank'><img src='".get_stylesheet_directory_uri()."/images/social/stumbleupon.png'></a></li>";
		}
		if( $tumblr != '' ) {
			$social_media[] = "<li><a href='{$tumblr}' target='_blank'><img src='".get_stylesheet_directory_uri()."/images/social/tumblr.png'></a></li>";
		}
		
		if( count( $social_media ) > 0 ) {
			
		echo '<div class="site-main new"><div class="hentry"><div class="entry-content">';
		echo '<ul class="social-media">';
			echo implode('', $social_media);
		echo '</ul>';
		echo '</div></div></div>';
		
		}

	// Gallery
	if ( function_exists( 'envira_gallery' ) ) { 
		$gallery = envira_gallery( get_the_ID(), 'id', array(), true);
		if( $gallery ) {
			echo '<div class="site-main new"><div class="hentry"><div class="entry-content">';
				echo $gallery;
			echo '</div></div></div>';
		}
	}

?>				

	</div><!-- .content-area -->



<?php get_footer(); ?>
