<?php

$itemscope = get_post_meta(get_the_ID(), '_establishment_details_itemscope', true);
$itemscope = ($itemscope!='') ? $itemscope : 'LocalBusiness';
?>

<article itemscope itemtype="http://schema.org/<?php echo $itemscope; ?>" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
		// Post thumbnail.
		indavao_post_thumbnail();
	?>

	<header class="entry-header">
		<?php
			if ( is_single() ) :
				the_title( '<h1 itemprop="name" class="entry-title">', '</h1>' );
			else :
				the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
			endif;
		?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php 
		
		/* Details */
		
		$address = get_post_meta(get_the_ID(), '_establishment_details_address', true); 
		$address_city = get_post_meta(get_the_ID(), '_establishment_details_address_city', true);
		$address_region = get_post_meta(get_the_ID(), '_establishment_details_address_region', true);
		$address_postal = get_post_meta(get_the_ID(), '_establishment_details_address_postal', true);
		if( $address != '' ) {
			echo "<p itemprop=\"address\" itemscope itemtype=\"http://schema.org/PostalAddress\">
			<strong>Address</strong>: <span itemprop=\"streetAddress\">{$address}</span>,
			<span itemprop=\"addressLocality\">{$address_city}</span>,
			<span itemprop=\"addressRegion\">{$address_region}</span>,
			<span itemprop=\"addressCountry\">Philippines</span>,
			<span itemprop=\"postalCode\">{$address_postal}</span>
			</p>";
		}
		
		$phone = get_post_meta(get_the_ID(), '_establishment_details_phone', true);
		if( $phone != '' ) {
			echo "<p><strong>Telephone Number(s)</strong>: <span itemprop=\"telephone\">{$phone}</span></p>";
		} 
		$mobile = get_post_meta(get_the_ID(), '_establishment_details_mobile', true); 
		if( $mobile != '' ) {
			echo "<p><strong>Mobile Number(s)</strong>: {$mobile}</p>";
		}
		$fax = get_post_meta(get_the_ID(), '_establishment_details_fax', true); 
		if( $fax != '' ) {
			echo "<p><strong>Fax Number(s)</strong>: {$fax}</p>";
		}
		$website = get_post_meta(get_the_ID(), '_establishment_details_website', true); 
		if( $website != '' ) {
			echo "<p><strong>Website</strong>: <a href=\"{$website}\" itemprop=\"url\" target=\"_blank\" rel=\"nofollow\">{$website}</a></p>";
		}
		
		?>

	</div><!-- .entry-content -->

	<?php
		// Author bio.
		/*
		if ( is_single() && get_the_author_meta( 'description' ) ) :
			get_template_part( 'author-bio' );
		endif;
		*/
	?>
<!--
	<footer class="entry-footer">
		<?php //twentyfifteen_entry_meta(); ?>
		<?php edit_post_link( __( 'Edit', 'twentyfifteen' ), '<span class="edit-link">', '</span>' ); ?>
	</footer>
-->
	<!-- .entry-footer -->

</article><!-- #post-## -->
